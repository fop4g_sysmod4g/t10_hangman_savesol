#include <assert.h>
#include <fstream>
#include <sstream>

#include "Game.h"

using namespace std;
using namespace sf;

void Game::Init()
{
	srand(0);
	if (!font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);
	ifstream file("data/save.txt", ios_base::in);
	if (file.is_open())
	{
		file >> bestName >> bestScore;
		assert(!file.bad());
		file.close();
	}
	else
	{
		bestName = "";
		bestScore = 0;
	}

}

void Game::HandleInput(Uint32 key_, RenderWindow& window)
{
	key = key_;
	if (key == GC::ESCAPE_KEY)
		window.close();
	if (mode==Mode::ENTER_NAME && (isdigit(key) || isalpha(key)))
		name += static_cast<char>(key);
}

void Game::Update(RenderWindow& window)
{
	switch (mode)
	{
	case Mode::ENTER_NAME:
		if (key == GC::ENTER_KEY && !name.empty())
			mode = Mode::WELCOME;
		break;
	case Mode::WELCOME:
		UpdateWelcome();
		break;
	case Mode::GUESS:
		UpdateGuess();
		break;
	case Mode::GAME_OVER:
		if (key == GC::ENTER_KEY)
			window.close();
		break;
	default:
		break;
	}
}

bool Game::PickNewWord()
{
	if (usedWordIds.size() == GC::MAX_WORDS)
		return false;
	bool busy;
	do
	{
		busy = false;
		tgtWordIdx = rand() % 5;
		for (int i = 0; i < (int)usedWordIds.size(); ++i)
			if (usedWordIds[i] == tgtWordIdx)
				busy = true;
	} while (busy);
	guess = "";
	for (int i = 0; i < (int)GC::WORDS[tgtWordIdx].length(); ++i)
		guess += "_ ";
	usedWordIds.push_back(tgtWordIdx);
	return true;
}

void Game::UpdateWelcome()
{
	if (key == GC::ENTER_KEY)
	{
		if (!PickNewWord())
			assert(false);
		mode = Mode::GUESS;
	}
}

void Game::CheckNewGuess()
{
	bool done = true;
	bool found = false;
	for (int i = 0; i < (int)GC::WORDS[tgtWordIdx].length(); ++i)
	{
		char tc = GC::WORDS[tgtWordIdx][i];
		if (tc == key)
		{
			found = true;
			guess[i * 2] = key;
		}

		if (tc != guess[i * 2])
			done = false;
	}
	usedLetters.push_back(key);
	if (!found)
	{
		lives--;
		if (lives == 0)
		{
			won = false;
			mode = Mode::GAME_OVER;
		}
	}
	if (done)
	{
		won = true;
		mode = Mode::GUESS;
		score++;
		usedLetters.clear();
		lives = GC::MAX_LIVES;
		if (!PickNewWord())
			mode = Mode::GAME_OVER;
	}
}

void Game::RoundOver()
{
	if (mode == Mode::GAME_OVER && score > bestScore)
	{
		bestScore = score;
		bestName = name;
		ofstream file("data/save.txt", ios_base::out);
		if (file.is_open())
		{
			file << bestName << ' ' << bestScore;
			assert(file.good());
			file.close();
		}
	}
}

void Game::UpdateGuess()
{
	if (key != 0)
	{
		//should we ignore this one?
		bool usedBefore = false;
		for (int i = 0; i < (int)usedLetters.size(); ++i)
			if (usedLetters[i] == key)
				usedBefore = true;
		if (!usedBefore)
		{
			CheckNewGuess();
			RoundOver();
		}
	}
}

//ask the use what their name is
void Game::RenderEnterName(RenderWindow& window)
{
	string msg = "Enter your name (press return): ";
	msg += name;
	Text txt2(msg, font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.1f);
	window.draw(txt2);
}

void Game::RenderWelcome(RenderWindow& window)
{
	stringstream sstr;
	sstr << "Welcome " << name;
	sstr << "\nTry and guess each word presented,\nyou can make seven mistakes per word. <press return>";
	sstr << "\nThe best score is " << bestScore << " by " << bestName;

	Text txt2(sstr.str(), font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.1f);
	window.draw(txt2);
}

void Game::RenderGuess(RenderWindow& window)
{
	stringstream sstr;
	sstr << "Lives left: " << lives << "\nScore: " << score << "\nYour guess: " << guess;
	Text txt2(sstr.str(), font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.15f);
	window.draw(txt2);
}

void Game::RenderGameOver(RenderWindow& window)
{
	stringstream sstr;
	sstr << "Target word: " << GC::WORDS[tgtWordIdx];

	if (won)
		sstr << "\nCongratulations, all words guessed OK :)";
	else
		sstr << "\nSorry, you lost :(";

	sstr << "\nThe best score is " << bestScore << " by " << bestName;
	sstr << "\nPress return to quit";

	Text txt(sstr.str(), font, 20);
	txt.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.15f);
	window.draw(txt);
}

void Game::Render(RenderWindow& window)
{
	Text txt("Hangman", font, 30);
	txt.setFillColor(Color::White);
	txt.setOutlineColor(Color::Red);
	txt.setOutlineThickness(5);
	txt.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.05f);
	window.draw(txt);

	switch (mode)
	{
	case Mode::ENTER_NAME:
		RenderEnterName(window);
		break;
	case Mode::WELCOME:
		RenderWelcome(window);
		break;
	case Mode::GUESS:
		RenderGuess(window);
		break;
	case Mode::GAME_OVER:
		RenderGameOver(window);
		break;
	default:
		assert(false);
	}
	key = 0;
}
