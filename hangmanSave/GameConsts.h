#pragma once

#include <string>

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};

 
/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width, 
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 1200,800 };

	const char ESCAPE_KEY{ 27 };
	const char ENTER_KEY{ 13 };

	const char NO_KEY = 0;		//default nothing was pressed
	const char MAX_LIVES = 7;	//how many failed guesses before death
	const int MAX_WORDS = 5;	//words in the library
	const std::string WORDS[MAX_WORDS]{	//library of words
		"hello",
		"goodbye",
		"muppet",
		"monkey",
		"superman"
	};
}


